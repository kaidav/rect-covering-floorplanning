from pypblib import pblib
from pysat.solvers import CNFPlus, Solver
import sys

pb2 = pblib.Pb2cnf()


# class which represents a Rectangle with all the variables defined for rectangles
class Rect:
    def __init__(self, n, m, var_offset):
        self.lefts = list(range(var_offset, var_offset + n))
        var_offset = var_offset + n
        self.rights = list(range(var_offset, var_offset + n))
        var_offset = var_offset + n
        self.ups = list(range(var_offset, var_offset + m))
        var_offset = var_offset + m
        self.downs = list(range(var_offset, var_offset + m))
        var_offset = var_offset + m
        self.horizontal = list(range(var_offset, var_offset + n))
        var_offset = var_offset + n
        self.vertical = list(range(var_offset, var_offset + m))

    def __str__(self) -> str:
        return "l=" + self.lefts.__str__() + "\n" + \
               "r=" + self.rights.__str__() + "\n" + \
               "u=" + self.ups.__str__() + "\n" + \
               "d=" + self.downs.__str__() + "\n" + \
               "h=" + self.horizontal.__str__() + "\n" + \
               "v=" + self.vertical.__str__()

    # constraint stating that exactly one variable in 'variables' is set to 1
    @staticmethod
    def __exactly_1_constraint(variables) -> pblib.PBConstraint:
        literals = []
        for variable in variables:
            literals.append(pblib.WeightedLit(variable, 1))
        return pblib.PBConstraint(literals, pblib.BOTH, 1, 1)

    # constraint stating that there must be a one in 'interval_vars[i]' if i is after the single 1 in start_vars and
    # before the single 1 in 'end_vars'
    @staticmethod
    def __interval_constraints(interval_vars, start_vars, end_vars) -> [pblib.PBConstraint]:
        interval_literals = []
        for var in interval_vars:
            interval_literals.append(pblib.WeightedLit(var, 1))

        constraints = []
        for j in range(0, len(interval_literals)):
            constraint_start = pblib.PBConstraint([interval_literals[j]], pblib.BOTH, 1)
            constraint_start.add_conditional(start_vars[j])
            if j < len(interval_literals) - 1:
                constraint_leading_0s = pblib.PBConstraint([pblib.WeightedLit(start_vars[j + 1], 1)], pblib.BOTH, 0)
                constraint_leading_0s.add_conditional(interval_vars[j])
                constraints.append(constraint_leading_0s)
                constraint_consecutive_1s = pblib.PBConstraint([interval_literals[j + 1],
                                                                pblib.WeightedLit(end_vars[j], 1)], pblib.BOTH, 1)
            else:
                constraint_consecutive_1s = pblib.PBConstraint([pblib.WeightedLit(end_vars[j], 1)], pblib.BOTH, 1)

            constraint_consecutive_1s.add_conditional(interval_vars[j])
            constraints.extend([constraint_start, constraint_consecutive_1s])
        return constraints

    # encode that the rectangle has exactly 4 bounds (left,right,up,down)
    def encode_unique_endpoint(self, formula, aux_var):
        pb2.encode(self.__exactly_1_constraint(self.lefts), formula, aux_var)
        pb2.encode(self.__exactly_1_constraint(self.rights), formula, aux_var)
        pb2.encode(self.__exactly_1_constraint(self.downs), formula, aux_var)
        pb2.encode(self.__exactly_1_constraint(self.ups), formula, aux_var)

    # encode the constraint for 'horizontal' and 'vertical' w.r. lefts,rights,ups and downs
    def encode_intervals(self, formula, aux_var):
        constraints = self.__interval_constraints(self.horizontal, self.lefts, self.rights)
        constraints.extend(self.__interval_constraints(self.vertical, self.downs, self.ups))
        encode_all(constraints, formula, aux_var)

    # print the value of the variables associated with the variables stored for the rectangle
    def print_rect_model(self, model):
        for row_idx in range(0, len(self.vertical)):
            for col_idx in range(0, len(self.horizontal)):
                if model[self.horizontal[col_idx] - 1] > 0 and model[self.vertical[row_idx] - 1] > 0:
                    print("1", end='')
                else:
                    print("0", end='')
            print()


# encode a list of constraints
def encode_all(constraints, formula, aux_var):
    for constraint in constraints:
        pb2.encode(constraint, formula, aux_var)


# helper function for printing a model
def print_vars(variables, model):
    for elem in variables:
        print(1 if model[elem - 1] > 0 else 0, end='')


# onstraint that 1s in the input matrix must be covered by at least one rectangle
def input_covered_constraints_rec(rects, literals, row_index, col_index) -> [pblib.PBConstraint]:
    if len(rects) == 0:
        return [pblib.PBConstraint(literals, pblib.GEQ, 1)]
    else:
        rect = rects[0]
        literal = pblib.WeightedLit(rect.horizontal[col_index], 1)
        constraints = input_covered_constraints_rec(rects[1:], literals + [literal], row_index, col_index)
        literal = pblib.WeightedLit(rect.vertical[row_index], 1)
        constraints.extend(input_covered_constraints_rec(rects[1:], literals + [literal], row_index, col_index))
        return constraints


# returns a constraint enforcing a 1 in the 'covered'- matrix if 'rect' has a 1 in the corresponding row AND column
def covered_constraint(rect, row_index, col_index, covered) -> pblib.PBConstraint:
    return pblib.PBConstraint([pblib.WeightedLit(rect.horizontal[col_index], -1),
                               pblib.WeightedLit(rect.vertical[row_index], -1),
                               pblib.WeightedLit(covered[row_index][col_index], 2)], pblib.GEQ, -1)


# main entry function, initially resetting all constraints and variables
def solve(k, ones, matrix) -> bool:
    n = len(matrix[0])
    m = len(matrix)
    config = pblib.PBConfig()
    formula = pblib.VectorClauseDatabase(config)
    rectangles = []
    for i in range(0, k):
        rectangles.append(Rect(n, m, 1 + i * (n + m) * 3))

    self_used_vars = k * (n + m) * 3
    aux_var = pblib.AuxVarManager(1 + self_used_vars)  # 1+k*(n+m)*2 is the number of already used variables

    # create the covered-matrix
    covered = []
    for row_idx in range(0, m):
        covered.append([])
        for col_idx in range(0, n):
            covered[row_idx].append(aux_var.get_variable())

    # encode constraints
    for r in rectangles:
        # encode the constraint that for each rectangle exactly two endpoints have to be set
        r.encode_unique_endpoint(formula, aux_var)
        # encode the constraints for the horizontal and the vertical boolean vectors
        r.encode_intervals(formula, aux_var)

    # encode the covered-constraints
    covered_literals = []
    for row_idx in range(0, m):
        for col_idx in range(0, n):
            if matrix[row_idx][col_idx] == 1:
                encode_all(input_covered_constraints_rec(rectangles, [], row_idx, col_idx), formula, aux_var)
            for r in rectangles:
                encode_all([covered_constraint(r, row_idx, col_idx, covered)], formula, aux_var)
            covered_literals.append(pblib.WeightedLit(covered[row_idx][col_idx], 1))
    # constrain the number of 1s in the covered-matrix
    pb2.encode(pblib.PBConstraint(covered_literals, pblib.LEQ, ones), formula, aux_var)

    # solve the problem using a SAT solver
    cnf = CNFPlus(from_string=formula.__str__())
    s = Solver(bootstrap_with=cnf)
    if s.solve():
        print("solution with %d" % ones)
        model = s.get_model()
        for r in rectangles:
            r.print_rect_model(model)
            print()
        print("c=\n", end='')
        for row in covered:
            print_vars(row, model)
            print()
        return True
    else:
        print("no solution with %d" % ones)
        return False


if __name__ == '__main__':
    mat = []
    if len(sys.argv) < 3:
        print("usage: %s <input-file> <k>" % sys.argv[0], file=sys.stderr)
        exit(-1)
    else:
        input_file = open(sys.argv[1], "r")
        for line in input_file:
            mat.append([])
            line = line.strip()
            if len(line) == 0:
                print("error file contains line without boolean values", file=sys.stderr)
                exit(-1)
            for c in line:
                if not c.isnumeric():
                    print("error file format must be a matrix of '0' and '1', but got char %c" % c, file=sys.stderr)
                    exit(-1)
                mat[len(mat) - 1].append(int(c))

    min_covered_cells = 0
    for rw in mat:
        min_covered_cells += sum(rw)

    covered_cells = min_covered_cells

    while not solve(int(sys.argv[2]), covered_cells, mat):
        covered_cells += 1
    print("solution with error of %d" % (covered_cells - min_covered_cells))
