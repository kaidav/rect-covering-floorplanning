# rect-covering-floorplanning

To run rectangle-cover.py, you must have external libraries pblib (https://github.com/master-keying/pblib) and pysat (https://pysathq.github.io/) installed:
run e.g. 
# pip3 install pypblib
# pip3 install pysat

After that you can simply invoke

# python3 rectangle-cover.py <input-file> <k>
